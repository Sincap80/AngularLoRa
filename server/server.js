"use strict";
exports.__esModule = true;
let express = require("express");
let http = require("http");
let WebSocket = require("ws");
let app = express();
let webs;
const fs = require("fs")

const {
    data,
    application
} = require("ttn")
const appID = "lora_measure_network"
const accessKey = "ttn-account-v2.iW16a3UV4TKCi5u7ppPQ9AL-owdyiZNGEjt9Gn0Ivbw"

const server = http.createServer(app);
const deviceList = require("./gauges.json")
const timeDiffrence = 3600000

const wss = new WebSocket.Server({
    server: server
});

wss.on('connection', function (ws) {
    console.log("connected")
    let extWs = ws;
    webs = ws;
    extWs.isAlive = true;
    ws.on('pong', function () {
        extWs.isAlive = true;
    });
    //connection is up, let's add a simple simple event
    ws.on('message', function (msg) {
        if (msg === "getData" && webs) {
            console.log(msg)
            webs.send(JSON.stringify({
                msg: 100,
                gauges: deviceList
            }, false));
        }
    });
    //send immediatly a feedback to the incoming connection    
    ws.on('error', function (err) {
        console.warn("Client disconnected - reason: " + err);
    });
});
setInterval(function () {
    wss.clients.forEach(function (ws) {
        let extWs = ws;
        if (!extWs.isAlive)
            return ws.terminate();
        extWs.isAlive = false;
        ws.ping(null, undefined);
    });
}, 10000);
//start our server
server.listen(process.env.PORT || 8999, function () {
    console.log("Server started on port " + server.address().port + " :)");
});

data(appID, accessKey)
    .then(function (client) {
        client.on("uplink", function (devID, payload) {
            payload.message = payload.payload_raw.toString("utf8")
            let date = new Date(payload.metadata.time)


            if (webs.isAlive) {
                console.log("Received uplink from devID")
                console.log(payload)

                let contains = false
                for (let dev of deviceList) {
                    if (dev.devId === devID) {
                        dev.stats.push({
                            date: date,
                            fuel: payload.message
                        })
                        contains = true
                        dev.fuel = payload.message
                    }
                }
                if (!contains)
                    deviceList.push({
                        devId: devID,
                        fuel: payload.message,
                        stats: [{
                            date: date,
                            fuel: payload.message
                        }],
                        timeStamp: date
                    })

                fs.writeFileSync("gauges.json", JSON.stringify(deviceList))
                webs.send(JSON.stringify({
                    msg: payload.message,
                    devId: devID
                }, false));
            }
        })
    })
    .catch(function (err) {
        console.error(err)
        process.exit(1)
    })

application(appID, accessKey)
    .then(function (client) {
        return client.get()
    })
    .then(function (app) {
        console.log("Got app", app)
        console.log("checking every hour for inactive devices")
        setInterval(function () {
            for (let dev of deviceList) {
                if ((Date.now() - dev.timeStamp) > timeDiffrence) {
                    if (webs) {
                        webs.send(JSON.stringify({
                            msg: 404,
                            devId: dev.id
                        }, false));
                        deviceList.splice(deviceList.indexOf(dev), 1)
                        console.log("DevId has not been sending any message since 1 Hour. Device deleted!")
                    }
                }
            }
        }, timeDiffrence);
    })
    .catch(function (err) {
        console.error(err)
        process.exit(1)
    })