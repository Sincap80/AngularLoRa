import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";

@Component({
  selector: 'app-stats',
  template: ''
})
export class StatsComponent implements OnInit {
  public chart = new Array<AmChart>();
  private devIds = new Array<string>();
  private divCount = 1;

  private socket$: WebSocketSubject<any>;

  constructor(private AmCharts: AmChartsService) { 
    this.socket$ = WebSocketSubject.create('ws://localhost:8999');
    this.socket$.next("getData")
    this.socket$.subscribe((message) => {      
      for(let gauge of message.gauges){
        this.addDivToHtml("statsdiv" + this.divCount)
        this.chart.push(this.getGauge(gauge.stats, "statsdiv" + this.divCount, gauge.devId))
        this.divCount++
        this.devIds.push(gauge.devId)
        }
    })
  }
  addDivToHtml(id) {
    let div = document.createElement("div")
    div.setAttribute("id", id)

    let attr = document.createAttribute("style")
    attr.value = "width: 50%; height: 300px; float:left; font-size: 12px"
    div.setAttributeNode(attr)

    div.className = "stats"
    document.body.appendChild(div)
  }
  getGauge(fuel, statsDiv, ID) {
    console.log(fuel)
    return this.AmCharts.makeChart(statsDiv, {
      "type": "serial",
      "theme": "light",
      "marginRight": 80,
      "dataProvider": fuel,
      "valueAxes": [{
          "position": "left",
          "title": ID
      }],
      "graphs": [{
          "id": "g1",
          "fillAlphas": 0.4,
          "valueField": "fuel",
           "balloonText": "<div style='margin:5px; font-size:19px;'>Fuellstand:<b>[[value]]</b></div>"
      }],
      "categoryField": "date",
      "categoryAxis": {
          "minPeriod": "mm",
          "parseDates": true
      }
  });
  
  }

  ngOnInit() {
    this.removeOldDivs()
  }
  removeOldDivs() {
    const oldCharts = document.getElementsByClassName("chartgauge").length
    for(let i = 1; i < oldCharts+1; i++){
      let el = document.getElementById("chartdiv"+i)
      if (el) el.remove()
    }
    console.log(document.getElementsByClassName("chartgauge"))
  }

}
