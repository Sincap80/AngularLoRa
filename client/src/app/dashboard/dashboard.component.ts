import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";

@Component({
  selector: 'app-dashboard',
  template: ''
})
export class DashboardComponent implements OnInit{
  public chart = new Array<AmChart>();
  private devIds = new Array<string>();
  private divCount = 1;

  private socket$: WebSocketSubject<any>;
  
  constructor(private AmCharts: AmChartsService) {
    this.socket$ = WebSocketSubject.create('ws://localhost:8999');
    this.socket$.next("getData")
    this.socket$.subscribe((message) => {
      if (parseInt(message.msg) === 100 && message.gauges) {
        for(let gauge of message.gauges){
          this.addDivToHtml("chartdiv" + this.divCount);
          let val = parseInt(gauge.fuel)
          this.chart.push(this.getGauge(val, 100 - val, "chartdiv" + this.divCount, gauge.devId))
          console.log(gauge)
          this.divCount++
          this.devIds.push(gauge.devId)
        }
      } else if (parseInt(message.msg) === 404) {
        this.removeGauge(message.devId)
      } else if (this.devIds.includes(message.devId)) {
        let a = this.devIds.indexOf(message.devId)
        let val = parseInt(message.msg)
        this.updateChart(val, 100 - val, a, message.devId)
      } else {
        this.addDivToHtml("chartdiv" + this.divCount);
        let val = parseInt(message.msg)
        this.chart.push(this.getGauge(val, 100 - val, "chartdiv" + this.divCount, message.devId))
        this.divCount++
        this.devIds.push(message.devId)
      }
    },
      (err) => console.error(err),
      () => console.warn('Completed!')
    );

  }
  removeGauge(devId) {
    const index = this.devIds.indexOf(devId)
    if (index > -1) {
      this.devIds.splice(index, 1)
      this.chart.splice(index, 1)
      const element = document.getElementById("chartdiv" + (index + 1))
      if (element) element.remove()
    }
  }
  addDivToHtml(id) {
    let div = document.createElement("div")
    div.setAttribute("id", id)

    let attr = document.createAttribute("style")
    attr.value = "width: 20%; height: 300px; float:left; font-size: 12px"
    div.setAttributeNode(attr)

    div.className = "chartgauge"
    document.body.appendChild(div)
  }
  updateChart(lower, upper, divCounter, ID) {
    this.AmCharts.updateChart(this.chart[divCounter], () => {
      this.chart[divCounter].dataProvider = [{
        "category": ID + "  " +lower + "%",
        "value1": lower,
        "value2": upper
      }]
    });
  }
  getGauge(lower, upper, chartDiv, ID) {
    return this.AmCharts.makeChart(chartDiv, {
      "theme": "light",
      "type": "serial",
      "depth3D": 100,
      "angle": 30,
      "autoMargins": true,
      "marginBottom": 100,
      "marginLeft": 10,
      "marginRight": 10,
      "dataProvider": [{
        "category": ID + "  " + lower + "%",
        "value1": lower,
        "value2": upper
      }],
      "valueAxes": [{
        "stackType": "100%",
        "gridAlpha": 0
      }],
      "graphs": [{
        "type": "column",
        "topRadius": 1,
        "columnWidth": 2,
        "showOnAxis": true,
        "lineThickness": 2,
        "lineAlpha": 0.5,
        "lineColor": "#FFFFFF",
        "fillColors": "#8d003b",
        "fillAlphas": 0.8,
        "valueField": "value1"
      }, {
        "type": "column",
        "topRadius": 1,
        "columnWidth": 2,
        "showOnAxis": true,
        "lineThickness": 2,
        "lineAlpha": 0.5,
        "lineColor": "#cdcdcd",
        "fillColors": "#cdcdcd",
        "fillAlphas": 0.5,
        "valueField": "value2"
      }],

      "categoryField": "category",
      "categoryAxis": {
        "axisAlpha": 0,
        "labelOffset": 40,
        "gridAlpha": 0
      },
      "export": {
        "enabled": true
      }
    });
  }
  ngOnInit() {
    this.removeOldDivs()
  }
  removeOldDivs() {
    const oldCharts = document.getElementsByClassName("stats").length
    for(let i = 1; i < oldCharts+1; i++){
      let el = document.getElementById("statsdiv"+i)
      if (el) el.remove()
    }
  }
  ngOnDestroy() {
    if (this.chart[0]) {
      this.AmCharts.destroyChart(this.chart[0]);
    }
  }
}
