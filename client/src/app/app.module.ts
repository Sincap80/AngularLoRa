import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatInputModule, MatButtonToggleModule, MatIconModule } from '@angular/material';

import { AppComponent } from './app.component';
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { StatsComponent } from './stats/stats.component';
import { AppRoutingModule } from './/app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
@NgModule({
    declarations: [
        AppComponent,
        StatsComponent,
        DashboardComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatIconModule,
        MatInputModule,
        MatCheckboxModule,
        AmChartsModule,
        AppRoutingModule
        
    ],
    providers: [],
    bootstrap: [AppComponent]
})

export class AppModule { }
